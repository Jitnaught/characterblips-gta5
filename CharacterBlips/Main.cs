﻿using GTA;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CharacterBlips
{
	public class CharacterBlips : Script
	{
		readonly BlipColor franklinColor, michaelColor, trevorColor;
		readonly BlipSprite franklinSprite, michaelSprite, trevorSprite;
		readonly string franklinName, michaelName, trevorName;

		Dictionary<int, Blip> charBlips = new Dictionary<int, Blip>();

		public CharacterBlips()
		{
			franklinColor = Settings.GetValue("Franklin", "Color", BlipColor.Green);
			franklinSprite = Settings.GetValue("Franklin", "Sprite", BlipSprite.Standard);
			franklinName = Settings.GetValue("Franklin", "Name", "Franklin");

			michaelColor = Settings.GetValue("Michael", "Color", BlipColor.Blue);
			michaelSprite = Settings.GetValue("Michael", "Sprite", BlipSprite.Standard);
			michaelName = Settings.GetValue("Michael", "Name", "Michael");

			trevorColor = Settings.GetValue("Trevor", "Color", BlipColor.Orange);
			trevorSprite = Settings.GetValue("Trevor", "Sprite", BlipSprite.Standard);
			trevorName = Settings.GetValue("Trevor", "Name", "Trevor");

			Interval = 1000;
			Tick += CharacterBlips_Tick;
			Aborted += CharacterBlips_Aborted;
		}

		private void CharacterBlips_Aborted(object sender, EventArgs e)
		{
			//delete all blips
			foreach (Blip blip in charBlips.Values)
			{
				blip.Remove();
			}

			charBlips.Clear();
		}

		void CharacterBlips_Tick(object sender, EventArgs e)
		{
			Ped plrPed = Game.Player.Character;

			if (plrPed == null || !plrPed.Exists() || plrPed.IsDead) return;

			HandleDeadBlips();
			HandleNewBlips();
		}

		void HandleDeadBlips()
		{
			var charBlipsList = charBlips.ToList();

			foreach (var charBlip in charBlipsList)
			{
				if (charBlip.Value == null || !charBlip.Value.Exists())
				{
					charBlips.Remove(charBlip.Key);
				}
			}
		}

		void HandleNewBlips()
		{
			Ped[] peds = World.GetAllPeds();

			foreach (Ped ped in peds)
			{
				if (ped == null || !ped.Exists()) continue; //skip if doesn't exist

				bool hasAttachedBlip = charBlips.ContainsKey(ped.Handle);

				if (ped == Game.Player.Character) //skip if ped is current player
				{
					if (hasAttachedBlip) //delete blip if attached to current player
					{
						charBlips[ped.Handle].Remove(); //delete blip
						charBlips.Remove(ped.Handle); //remove blip from Dictionary
					}

					continue;
				}

				if (hasAttachedBlip) continue; //skip if have blip attached already

				Model model = ped.Model;

				bool isFranklin = model == PedHash.Franklin,
					isMichael = model == PedHash.Michael,
					isTrevor = model == PedHash.Trevor;

				if (!isFranklin && !isMichael && !isTrevor) continue; //skip if not a player character
				if (GetPedBlip(ped) != null) continue; //skip if already has any blip
				
				Blip blip = ped.AddBlip();

				if (isFranklin)
				{
					blip.Sprite = franklinSprite;
					blip.Color = franklinColor;
					blip.Name = franklinName;
				}
				else if (isMichael)
				{
					blip.Sprite = michaelSprite;
					blip.Color = michaelColor;
					blip.Name = michaelName;
				}
				else if (isTrevor)
				{
					blip.Sprite = trevorSprite;
					blip.Color = trevorColor;
					blip.Name = trevorName;
				}

				charBlips.Add(ped.Handle, blip);
			}
		}

		Blip GetPedBlip(Ped ped)
		{
			int handle = Function.Call<int>(Hash.GET_BLIP_FROM_ENTITY, ped.Handle);

			if (Function.Call<bool>(Hash.DOES_BLIP_EXIST, handle))
			{
				return new Blip(handle);
			}

			return null;
		}
	}
}
